class Product {
  constructor(make, model, count, price) {
    this.make  = make;
    this.model = model;
    this.count = count;
    this.price = price;
  }

  getInfo() {
    return "Make: " + this.make + "\nModel: " + this.model+ "\nCount: " + this.count + "\nPrice: " + this.price + "р";
  }

  getTotalPrice() {
    return this.price * this.count;
  }

}

class CellPhone extends Product {
  constructor(make, model, count, price, camera, memory) {
    super(make, model, count, price);
    this.camera = camera;
    this.memory = memory;
  }

  getInfo() {
    return super.getInfo() + "\nCamera: " + this.camera + "mb" + "\nMemory: " + this.memory + "gb";
  }

  getTotalPrice() {
    var basePrice = super.getTotalPrice();
    return basePrice + basePrice * this.memory / 100;
  }

}

class Printer extends Product {
  constructor(make, model, count, price, printSpeed, paperTrayCapacity) {
    super(make, model, count, price);
    this.printSpeed = printSpeed;
    this.paperTrayCapacity = paperTrayCapacity;
  }

  getInfo() {
    return super.getInfo() + "\nPrint speed: " + this.printSpeed + " pages" + "\nPaper tray capacity: " + this.paperTrayCapacity + " pages per minute";
  }

}

class WashingMachine extends Product {
  constructor(make, model, count, price, maxLoad, spinSpeed) {
    super(make, model, count, price);
    this.maxLoad = maxLoad;
    this.spinSpeed = spinSpeed;
  }

  getInfo() {
    return super.getInfo() + "\nMax load: " + this.maxLoad + " kg" + "\nSpin Speed: " + this.spinSpeed + " rev / min";
  }

}

var products = [
  new Printer('Hewlett-Packard Company', 'CE651A', 3, 250, 18, 150),
  new CellPhone('Sony', 'Xperia Z3', 1, 800, 20.7, 16),
  new WashingMachine('Samsung', 'WF8590NLW9DYLP', 10, 680, 6, 1000),
  new Printer('Hewlett-Packard Company', 'CE4674', 20, 300, 21, 180),
  new CellPhone('Sony', 'Xperia Z4', 5, 1000, 25, 64),
  new WashingMachine('Samsung', 'WF8260NLW9DYLP', 6, 760, 7, 1200)
];

products.forEach( function(product, i, products) {
    alert( product.getInfo() );
    alert( product.getTotalPrice() );
});
