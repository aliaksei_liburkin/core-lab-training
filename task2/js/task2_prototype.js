/*Class Product*/
function Product(make, model, count, price) {
  this.make  = make;
  this.model = model;
  this.count = count;
  this.price = price;
}

/*Product methods*/
Product.prototype.getInfo = function() {
  return "Make: " + this.make + "\nModel: " + this.model + "\nCount: " + this.count + "\nPrice: " + this.price + "р";
};

Product.prototype.getTotalPrice = function() {
  return this.price * this.count;
};

/*Class CellPhone*/
function CellPhone(make, model, count, price, camera, memory) {
  Product.call(this, make, model, count, price);
  this.camera = camera;
  this.memory = memory;
}

/*Class Printer*/
function Printer(make, model, count, price, printSpeed, paperTrayCapacity) {
  Product.call(this, make, model, count, price);
  this.printSpeed = printSpeed;
  this.paperTrayCapacity = paperTrayCapacity;
}

/*Class WashingMachine*/
function WashingMachine(make, model, count, price, maxLoad, spinSpeed) {
  Product.call(this, make, model, count, price);
  this.maxLoad = maxLoad;
  this.spinSpeed = spinSpeed;
}

/*Inheritance*/
CellPhone.prototype = Object.create(Product.prototype);
Printer.prototype = Object.create(Product.prototype);
WashingMachine.prototype = Object.create(Product.prototype);

/*CellPhone - method getInfo override*/
CellPhone.prototype.getInfo = function() {
  return Product.prototype.getInfo.apply(this) + "\nCamera: " + this.camera + "mb" + "\nMemory: " + this.memory + "gb";
};

/*CellPhone - method getTotalPrice override*/
CellPhone.prototype.getTotalPrice = function() {
  var basePrice = Product.prototype.getTotalPrice.apply(this);
  return basePrice + basePrice * this.memory / 100;
};

/*Printer - method getInfo override*/
Printer.prototype.getInfo = function() {
  return Product.prototype.getInfo.apply(this) + "\nPrint speed: " + this.printSpeed + " pages" + "\nPaper tray capacity: " + this.paperTrayCapacity + " pages per minute";
};

/*WashingMachine - method getInfo override*/
WashingMachine.prototype.getInfo = function() {
  return Product.prototype.getInfo.apply(this) + "\nMax load: " + this.maxLoad + " kg" + "\nSpin Speed: " + this.spinSpeed + " rev / min";
};

var products = [
  new Printer('Hewlett-Packard Company', 'CE651A', 3, 250, 18, 150),
  new CellPhone('Sony', 'Xperia Z3', 1, 800, 20.7, 16),
  new WashingMachine('Samsung', 'WF8590NLW9DYLP', 10, 680, 6, 1000),
  new Printer('Hewlett-Packard Company', 'CE4674', 20, 300, 21, 180),
  new CellPhone('Sony', 'Xperia Z4', 5, 1000, 25, 64),
  new WashingMachine('Samsung', 'WF8260NLW9DYLP', 6, 760, 7, 1200)
];

products.forEach( function(product, i, products) {
  alert( product.getInfo() );
  alert( product.getTotalPrice() );
});
