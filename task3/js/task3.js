/*_________________________________Classes___________________________________*/

class Product {
  constructor(make, model, count, price) {
    this.make  = make;
    this.model = model;
    this.count = count;
    this.price = price;
  }

  toJSON() {
    return {
      make:  this.make,
      model: this.model,
      count: this.count,
      price: this.price
    };
  }

  getInfo() {
    return "Make: " + this.make + "\nModel: " + this.model+ "\nCount: " + this.count + "\nPrice: " + this.price + " r.";
  }

  getTotalPrice() {
    return this.price * this.count;
  }

}

class CellPhone extends Product {
  constructor(make, model, count, price, camera, memory) {
    super(make, model, count, price);
    this.camera = camera;
    this.memory = memory;
  }

  toJSON() {
    var product = super.toJSON();
    product.camera = this.camera;
    product.memory = this.memory;

    return {
        "CellPhone":  product
    }
  }

  getInfo() {
    return super.getInfo() + "\nCamera: " + this.camera + "mb" + "\nMemory: " + this.memory + "gb";
  }

  getTotalPrice() {
    var basePrice = super.getTotalPrice();
    return basePrice + basePrice * this.memory / 100;
  }

}

class Printer extends Product {
  constructor(make, model, count, price, printSpeed, paperTrayCapacity) {
    super(make, model, count, price);
    this.printSpeed = printSpeed;
    this.paperTrayCapacity = paperTrayCapacity;
  }


  toJSON() {
    var product = super.toJSON();
    product.printSpeed = this.printSpeed;
    product.paperTrayCapacity = this.paperTrayCapacity;

    return {
        "Printer":  product
    }
  }

  getInfo() {
    return super.getInfo() + "\nPrint speed: " + this.printSpeed + " pages" + "\nPaper tray capacity: " + this.paperTrayCapacity + " pages per minute";
  }

}

class WashingMachine extends Product {
  constructor(make, model, count, price, maxLoad, spinSpeed) {
    super(make, model, count, price);
    this.maxLoad = maxLoad;
    this.spinSpeed = spinSpeed;
  }

  toJSON() {
    var product = super.toJSON();
    product.maxLoad = this.maxLoad;
    product.spinSpeed = this.spinSpeed;

    return {
        "WashingMachine":  product
    }
  }

  getInfo() {
    return super.getInfo() + "\nMax load: " + this.maxLoad + " kg" + "\nSpin Speed: " + this.spinSpeed + " rev / min";
  }

}


/*_________________________________Data Module___________________________________*/

var dataModule = ( function () {

    var jsonStr = '[{"Printer":{"make":"Hewlett-Packard Company","model":"E651A","count":3,"price":250,"printSpeed":18,"paperTrayCapacity":150}},{"CellPhone":{"make":"Sony","model":"Xperia Z3","count":1,"price":800,"camera":20.7,"memory":16}},{"WashingMachine":{"make":"Samsung","model":"WF8590NLW9DYLP","count":10,"price":680,"maxLoad":6,"spinSpeed":1000}},{"Printer":{"make":"Hewlett-Packard Company","model":"CE4674","count":20,"price":300,"printSpeed":21,"paperTrayCapacity":180}},{"CellPhone":{"make":"Sony","model":"Xperia Z4","count":5,"price":1000,"camera":25,"memory":64}},{"WashingMachine":{"make":"Samsung","model":"WF8260NLW9DYLP","count":6,"price":760,"maxLoad":7,"spinSpeed":1200}}]';

    return {

        getData: function() {
            return jsonStr;
        }

    };

})();


/*_________________________________Parse Module___________________________________*/

var parseModule = ( function() {

    return {
        parse: function(json) {
            return JSON.parse(json);
        }
    };

})();


/*_________________________________Main Module___________________________________*/

var mainModule = ( function() {

    var json = dataModule.getData();
    var products = parseModule.parse(json);

    return {
        printResult: function() {

            products.forEach( function(product) {

                var es6ProductObject = factoryModule.getProduct(product);

                console.log( es6ProductObject.getInfo() );
                console.log("Total price: " + es6ProductObject.getTotalPrice() + " r."  );

            })
        }
    };

})();


/*_________________________________Factory Module___________________________________*/

var factoryModule = ( function() {

    const classes = { Printer, WashingMachine, CellPhone };

    function dynamicClass (name) {
      return classes[name];
    }

    return {
        getProduct: function(product) {

            const constructor = dynamicClass(Object.keys(product));
            var newObj = new constructor();
            Object.assign(newObj,product[Object.keys(product)]);

            return newObj;
        }
    };

})();


/*_________________________________Print Result___________________________________*/

mainModule.printResult();
