
 $(document).ready(function () {

    var total_price = 0;

    $.ajax({
        url: 'http://www.json-generator.com/api/json/get/crfAiwPYmW?indent=2',
        dataType : "json",
        success: function (data) {

            var books = '';

            /*loop for each book*/
            $.each(data, function (i, val) {
                books += "<div class='book' type='" + val.type + "'><div class='front'><img src='" + val.image + "' width='230' height='400'></div><div class='back'><p class='book-title'>" + val.name + "</p><p class='book-author'>" + val.author + "</p><p class='book-description'>" + val.intro + "</p><p class='text-center book-price'>$" + val.price + "</p><button type='button' class='buy-button btn btn-success btn-block fa-1x'><i class='fa fa-shopping-cart fa-1-2x' aria-hidden='true'></i> Buy</button></div></div>";
            });

            /*write book in DOM*/
            $('div#books').html(books);

        },
        error: function () {
            $('div#books').html("<div class='alert alert-danger' role='alert'>Sorry, can't find book, please try again later.</div>");
        }
    });

    /*sorting*/
    $("#sort .btn-group").on('click', '.btn', function () {
        var type = $(this).text().trim().toLowerCase();
        $(".book").each(function () {
            if (type === 'all') {
                $(this).css("display", "");
            } else if ($(this).attr('type') !== type) {
                 $(this).css("display", "none");
            } else {
                $(this).css("display", "");
            }
        });
    });

    /*buy books*/
    $("body").on('click', '.buy-button', function () {
        var type = $(this).prev(".book-price").text();
        var number = Number(type.replace(/[^0-9\.]+/g, ""));
        total_price += number;
        $('#total_price').html('$' + Math.round(total_price * 100) / 100);
    });

    /*book hover action*/
    $("#books").on({
        mouseenter: function () {
            jQuery(".back", this).css("display", "block");
            jQuery(".front img", this).stop(true, true).animate({
                width: "+=20%",
                height: "+=20%"
            }, 400);
        },
        mouseleave: function () {
            jQuery(".back", this).css("display", "none");
            jQuery(".front img", this).stop(true, true).animate({
                width: "-=20%",
                height: "-=20%"
            }, 400);
        }
    }, '.book');

});
