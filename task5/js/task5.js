(function ($) {

    var index = 0;

    $.fn.gallery = function (options) {

        var obj = this;


        /*COMPONENTS*/
        var buttons = '<div class="buttons"><a href="#" class="prev glyphicon glyphicon-chevron-left fa-2x" ></a><a href="#" class="next glyphicon glyphicon-chevron-right fa-2x"></a></div>';
        var slidesWrapper = '<div class="slides"><ul></ul></div>';
        var imageWrapper = '<li></li>';
        var indicators = '';

        $(this).find($("img")).each(function () {
            index += 1;
            var id = 'img' + '_' + index;
            $(this).attr('id', id);
            $(this).wrap(imageWrapper);
            indicators += '<label for="' + id + '" class="indicator"></label>';
        });

        $(this).find($('li')).wrapAll(slidesWrapper);


        if ($(this).find($(".buttons")).length === 0) {
            $(this).append(buttons);
        }

        if ($(this).find($(".indicators")).length === 0) {
            $(this).append('<div class="indicators">'+ indicators + '</div>');
        }


        /*SETTINGS*/
        var defaults = {
            indicatorsColor: "#000",
            animationDuration: 500
        };
        var settings = $.extend({}, defaults, options);

        /*CHANGE INDICATORS COLOR*/
        $('.indicators .indicator', this).css('background-color', settings.indicatorsColor);

        var currentSlide = $('.slides li', this);
        var slides = $('.slides ul', this);
        var item_width = currentSlide.outerWidth();
        var left_value = item_width * (-1);

        $('.slides li:first', this).before($('.slides li:last', this));
        slides.css({'left' : left_value});

        /*PREV BUTTON ACTION */
        $('.prev', this).click(function () {

            var left_indent = parseInt(slides.css('left')) + item_width;

            slides.stop(true, true).animate({'left' : left_indent}, settings.animationDuration, function () {
                $('.slides li:first', obj).before($('.slides li:last', obj));
                slides.css({'left' : left_value});
            });

            return false;

        });

        /*NEXT BUTTON ACTION */
        $('.next', this).click(function () {

            var left_indent = parseInt(slides.css('left')) - item_width;
            slides.stop(true, true).animate({'left' : left_indent}, settings.animationDuration, function () {
                $('.slides li:last', obj).after($('.slides li:first', obj));
                slides.css({'left' : left_value});
            });

            return false;

        });

        /*INDICATORS ACTION */
        $('.indicator', this).click(function () {

            var int = '#' + $(this).attr("for");

            slides.animate(settings.animationDuration, function () {
                $('.slides li:nth-child(2)', obj).swap($('.slides ul li img' + int).parent());
            });

            return false;

        });
    };

    /*SWAP ELEMENTS*/
    $.fn.swap = function (elem) {
        elem = elem.jquery ? elem : $(elem);
        return this.each(function () {
            $(document.createTextNode('')).insertBefore(this).before(elem.before(this)).remove();
        });
    };

}(jQuery));



var options = {
  indicatorsColor: 'red',
  animationDuration: 600
};

var options1 = {
  indicatorsColor: 'blue',
  animationDuration: 1600
};


$("#img-gallery").gallery(options);

$("#img-gallery1").gallery(options1);


