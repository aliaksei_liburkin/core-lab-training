import React, { Component } from 'react';
import { Modal, Button, Form, FormGroup, FormControl, Col } from 'react-bootstrap';

class AddEmployeeModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: JSON.parse(localStorage.getItem('employees')),
        }
    }

    closeModal() {
        this.props.onChildChanged({ showModal: false });
    }

    addEmployee(event) {
        event.preventDefault();
        let firstName = this.firstName.value;
        let lastName = this.lastName.value;
        let jobPosition = this.jobPosition.value;
        let photo = this.photo.value;
        let id = 0;

        this.state.employees.forEach(function(item){
            if (item.id > id) {
                id = item.id;
            }
        });

        id += 1;

        let newEmployees = this.state.employees.concat({id, firstName, lastName, jobPosition, photo});

        this.setState({
            employees: newEmployees
        });

        this.props.onChildChanged({ employees: newEmployees });

        localStorage.setItem('employees', JSON.stringify(newEmployees));
    }

    openModal() {
        this.props.onChildChanged({ showModal: true });
    }

    render() {
        return (
                <Modal show={this.props.showModal} onHide={this.closeModal.bind(this)}>

                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>

                        <Form horizontal onSubmit={this.addEmployee.bind(this)}>

                            <FormGroup controlId="formHorizontalFirstName">
                                <Col sm={12}>
                                    <FormControl required inputRef={ ref => { this.firstName = ref; }} type="text" placeholder="First name" />
                                </Col>
                            </FormGroup>

                            <FormGroup controlId="formHorizontalLastName">
                                <Col sm={12}>
                                    <FormControl required inputRef={ ref => { this.lastName = ref; }} type="text" placeholder="Last name" />
                                </Col>
                            </FormGroup>

                            <FormGroup controlId="formHorizontalJobPosition">
                                <Col sm={12}>
                                    <FormControl required inputRef={ ref => { this.jobPosition = ref; }} type="text" placeholder="Job Position" />
                                </Col>
                            </FormGroup>

                            <FormGroup controlId="formHorizontalPhoto">
                                <Col sm={12}>
                                    <FormControl inputRef={ ref => { this.photo = ref; }} type="text" placeholder="Photo url" />
                                </Col>
                            </FormGroup>

                            <FormGroup>
                                <Col sm={12}>
                                    <Button type="submit" block bsStyle="success">
                                        Add Employee
                                    </Button>
                                </Col>
                            </FormGroup>

                      </Form>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.closeModal.bind(this)}>Close</Button>
                    </Modal.Footer>

                </Modal>
                );
    }
}

export default AddEmployeeModal;
