import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';
import EmployeesList from './EmployeesList';
import Header from './Header';
import SearchPanel from './SearchPanel';
import './css/index.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
            employees: JSON.parse(localStorage.getItem('employees')),
            showModal: false
        }
    }

    onChildChanged(obj) {
        this.setState(obj);
    }

    render() {

        let filteredEmployees = this.state.employees.filter(
            (contact) => {
                var fullName = contact.firstName + contact.lastName;
                return fullName.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
            }
        );

        return (

            <Grid fluid={true}>

                <Header/>

                <SearchPanel onChildChanged={this.onChildChanged.bind(this)} />

                <EmployeesList onChildChanged={this.onChildChanged.bind(this)} employees = {this.state.employees} filteredEmployees={filteredEmployees} />

            </Grid>
        );
    }
}

export default App;
