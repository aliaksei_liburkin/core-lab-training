import React, { Component } from 'react';
import { ListGroupItem, Button, Col, Row} from 'react-bootstrap';
import { Link  } from 'react-router'


class Employees extends Component {

    deleteEmployee (contact) {
        const newEmployees = this.props.employees;
        if (newEmployees.indexOf(contact) > -1) {
            newEmployees.splice(newEmployees.indexOf(contact), 1);
            this.props.onChildChanged({employees: newEmployees});
            localStorage.setItem('employees', JSON.stringify(newEmployees));
        }
    }

    render() {
        return (
                <ListGroupItem>
                    <Row className="display-table">
                            <Col xs={12} className="display-cell" >
                                <Link to={'/employee/' + this.props.contact.id}>
                                    <div className="card">
                                        <img src={this.props.contact.photo} className="img-rounded pull-left block-right" width="100" height="100"/>
                                        <h4>{this.props.contact.firstName} {this.props.contact.lastName}</h4>
                                        <p className="text-muted">{this.props.contact.jobPosition}</p>
                                    </div>
                                </Link>
                            </Col>
                        <Col xs={12} className="display-cell" >
                            <Button onClick={this.deleteEmployee.bind(this, this.props.contact)} className=" btn-round"><span className="fa fa-3x fa-trash-o"></span></Button>
                        </Col>
                    </Row>
                </ListGroupItem>
        );
    }
}

export default Employees;
