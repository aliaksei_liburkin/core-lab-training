import React, { Component } from 'react';

class EmployeeProfile extends Component {

    constructor(props){
        super(props);

        if (this.props.params){

            let id = this.props.params.employeeId;
            let employees = JSON.parse(localStorage.getItem('employees'));
            let employee;

            employees.forEach(function(item){
                if (item.id == id) {
                    employee = item;
                }
            });

            this.employee = employee;
        }
    }

    render() {
        if (this.employee){
            return (
                <div className="card text-center">
                    <img src={this.employee.photo} className="img-rounded" width="100" height="100"/>
                    <h4>{this.employee.firstName} {this.employee.lastName}</h4>
                    <p className="text-muted">{this.employee.jobPosition}</p>
                </div>
            );
        } else {
            return(
                <div className="card text-center">
                    Employee not found
                </div>
            );
        }
    }
}

export default EmployeeProfile;
