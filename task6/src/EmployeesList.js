import React, { Component } from 'react';
import { ListGroup, Row } from 'react-bootstrap';
import Employee from './Employee';

class EmployeesList extends Component {

    render() {
        return (
            <Row>
                <ListGroup fill>
                    {this.props.filteredEmployees.map( (contact) => {
                        return <Employee onChildChanged = {this.props.onChildChanged} filteredEmployees = {this.props.filteredEmployees} employees={this.props.employees} contact={contact} key={contact.id} />
                    })}
                </ListGroup>
            </Row>
        );
    }
}

export default EmployeesList;
