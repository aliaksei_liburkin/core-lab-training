import React, { Component } from 'react';
import { PageHeader } from 'react-bootstrap';

class Header extends Component {

    render() {
        return (
            <PageHeader className="text-center">Employee Directory</PageHeader>
        );
    }
}

export default Header;
