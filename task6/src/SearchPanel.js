import React, { Component } from 'react';
import { Row, Col, Button, FormControl } from 'react-bootstrap';
import AddEmployeeModal from './AddEmployeeModal';

class SearchPanel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
            showModal: false
        }
    }

    openModal() {
        this.setState({ showModal: true });
        this.props.onChildChanged({ showModal: true });
    }

    updateSearch(event) {
        let value = event.target.value.substr(0,20);
        this.setState({ search: value });
        this.props.onChildChanged({ search: value });
    }

    onChildChanged(obj) {
        this.setState(obj);
        this.props.onChildChanged(obj);
    }

    render() {
        return (
                <Row className="vertical-buffer" >

                    <Col xs={12} sm={10}>
                        <FormControl
                            type="text"
                            onChange={this.updateSearch.bind(this)}
                            value={this.state.search}
                            placeholder="Search by name..."
                        />
                    </Col>

                    <Col xs={12} sm={2}>
                        <Button onClick={this.openModal.bind(this)} bsStyle="success" block>Add Employee</Button>
                    </Col>

                    <AddEmployeeModal showModal={this.state.showModal} onChildChanged={this.onChildChanged.bind(this)} />
                </Row>


                );
    }
}

export default SearchPanel;
