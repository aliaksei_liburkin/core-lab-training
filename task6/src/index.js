import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import EmployeeProfile from './EmployeeProfile';
import { Router, Route, browserHistory } from 'react-router'

let employees = [{
    id: 1,
    firstName: 'James',
    lastName: 'King',
    jobPosition: 'President and CEO',
    photo: 'http://www.belcanto.ru/media/images/persons/thumbnail430_james-king.jpg'
},  {
    id: 2,
    firstName: 'Julia',
    lastName: 'Taylor',
    jobPosition: 'VP of Marketing',
    photo: 'http://cdn.wall-pix.net/cache/people-3d-girls/b9/b98f8008c4bc0328a628c776023c3f92927be85e.jpg'
}]

if ( ! localStorage.getItem('employees') ) {
    localStorage.setItem('employees', JSON.stringify(employees));
}

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App}/>
    <Route path="/employee/:employeeId" component={EmployeeProfile}/>
  </Router>
), document.getElementById('root'))
