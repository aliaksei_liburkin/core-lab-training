module.exports = {
    entry: [
        "./src/index.js"
    ],

    output: {
        path: __dirname,
        filename: "index.js"
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: "babel",
            query:
            {
                presets: ['es2015', 'react']
            }
        },
        { test: /\.css$/, loader: "style!css" }]
    },
    devServer: {
        historyApiFallback: true,
    },
};
